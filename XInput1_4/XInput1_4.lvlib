﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+X!!!*Q(C=\:;R&lt;B."%)&lt;`E%B1U*AH1";&gt;/_9";#R:!CF)#+?BHY)8=*P3;3/Z'#E3P2]AD1N+GKH3O[&lt;T+RT@LD?/(71H3%3C90&lt;G\P;@H&gt;HP^N9H3^6?3#@K(D)&gt;MA(R.`NN=&amp;0CX@7[F6.8,ZO\\@D&gt;I/P&lt;9RW`6&lt;PN`(,MZL@5B_/FU(&lt;]?K^&gt;F?=`P&gt;JP.Y?@`X1^`WFJ^[UI6XPC^,K\?/V7I6P@N?NO`G\&lt;%[_"&gt;@*7`2WGOU\8VP]X^)X&gt;T`]@@Y,YY0$_'^Q=MM@]P-]@(04%*BWJ@)DQR!.XX&amp;2#8;)H?K)H?K)H?K!(?K!(?K!(OK-\OK-\OK-\OK%&lt;OK%&lt;OK%&lt;?GXI1B?[U&amp;G6:0*EIK2I5C!:$)K35]+4]#1]#1^&gt;*4Q*4]+4]#1]$&amp;(#E`!E0!F0QE/;%J[%*_&amp;*?")?3F73L!U&gt;HI3(]AJY!J[!*_!*?*B3!5]!%%Q7&amp;![+A+%A'.Q%0!&amp;0Q-/N!J[!*_!*?!)?QAJY!J[!*_!*?%CJKR+6JD2U?#ADB]@B=8A=(I?(UH*Y("[(R_&amp;R?*B/$I`$YU!Y%TL&amp;1:#4Z!RQ/A[0Q].&amp;$I`$Y`!Y0!Y0I@K'P+Z-I3E.(2[$R_!R?!Q?AY=3-HA-(I0(Y$&amp;Y+#O$R_!R?!Q?AY?J:0!90!;0!7*-SP1SCBG*RC"$-(AY[G[R_J;CEFBN5NO];JN3&lt;&lt;/J&lt;3+VT;(WUN6?JNJ,5FN]N5667SSV26"\/$6I.2CV3&gt;33SU#N/#`R"4\(J`A%(_.$P)`X3OJ@(LB;L&lt;2=,L69,$3@TT7&gt;4D7:4$1?DT5=$N8P^^8L^4:`*V\4VH:U_VU[ZRY`HHWY`$H\_OHDR:@,U?T^+`TF#0XZN`79YZEUGOHERY7@D7&lt;DT[0:W&gt;P2_9H?F4&amp;`YK8?^]VX[6`Y.OK:OKW`9;T2,U-ZJ&lt;Q!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="subvi" Type="Folder">
		<Item Name="Error Converter (ErrCode or Status).vi" Type="VI" URL="../subvi/Error Converter (ErrCode or Status).vi"/>
	</Item>
	<Item Name="X Input Enable.vi" Type="VI" URL="../VIs/X Input Enable.vi"/>
	<Item Name="X Input Get Battery Information.vi" Type="VI" URL="../VIs/X Input Get Battery Information.vi"/>
	<Item Name="X Input Get Capabilities.vi" Type="VI" URL="../VIs/X Input Get Capabilities.vi"/>
	<Item Name="X Input Get Keystroke.vi" Type="VI" URL="../VIs/X Input Get Keystroke.vi"/>
	<Item Name="X Input Get State.vi" Type="VI" URL="../VIs/X Input Get State.vi"/>
	<Item Name="X Input Set State.vi" Type="VI" URL="../VIs/X Input Set State.vi"/>
</Library>
